FROM golang:1.12-alpine

COPY . /go/src/gitlab.com/craigfurman/aurite
ENV CGO_ENABLED=0
RUN go install gitlab.com/craigfurman/aurite

FROM base/devel:latest

COPY --from=0 /go/bin/aurite /usr/bin/aurite
