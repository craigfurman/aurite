.PHONY: dockerimage

DOCKER_IMAGE_TAG ?= $$(git rev-parse --short HEAD)

test:
	go test -race ./...

lint:
	gometalinter --tests --vendor --deadline 90s \
		--exclude "should have comment or be unexported" \
		--exclude "Expect file permissions to be 0600 or less" \
		./...

dockerimage:
	docker build --pull -t craigfurman/aurite:$(DOCKER_IMAGE_TAG) .
