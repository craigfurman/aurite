package repositories

import (
	"time"
)

type Package struct {
	Arch         string
	Base         string // TODO what is this? == pkgbase?
	BuildDate    time.Time
	CSize        int64 // Download size
	CheckDepends []string
	Conflicts    []string
	Depends      []string
	Desc         string
	Filename     string
	Groups       []string
	ISize        int64  // Installed size
	License      string // TODO array?
	MakeDepends  []string
	Md5Sum       string
	Name         string
	OptDepends   []string // in "name: desc" pairs. TODO struct?
	PGPSig       string
	Packager     string
	Provides     []string
	Replaces     []string
	Sha256Sum    string
	URL          string
	Version      string
}

// Example of a package tar entry:

// %FILENAME%
// bash-4.4.023-1-x86_64.pkg.tar.xz

// %NAME%
// bash

// %BASE%
// bash

// %VERSION%
// 4.4.023-1

// %DESC%
// The GNU Bourne Again shell

// %GROUPS%
// base

// %CSIZE%
// 1462576

// %ISIZE%
// 7496704

// %MD5SUM%
// c7a52b292da4c29cff27128f14c61c4a

// %SHA256SUM%
// d9f212d5c9a22d613891169dae0eab31c4d230fc9e2fa4da5fb399282e745b43

// %PGPSIG%
// iQEzBAABCAAdFiEE82kWh9hnuBtRzgfZu+Q3cUhzKKkFAlsU/zYACgkQu+Q3cUhzKKm7RAgAl2r27YlwoZjGcWmJd8NCHKEzI1nPrzwPY9fssnOQ/C5MojfrPHGucwpnp5VwVxTUyhvtAElx1qEo5aWzQuRYC3GpVvgeV5djG+MAxVJc41YuY+hv9ppfGpev8JXln0gckt89ottXfMziva0FhtccaukvwWA7nRrDiEN9hoOJMT87kftHe7UNF9q0aUMYlIzcTXR19dRF6jkE9CpxycvPeHTmd4y+BVsI27HA3xT5kwCAfxne4MRBHCdDkw1hPzvixmoCb4WXlDEXuaUE0nMFRYq01HQI8PIcpquOOhsS36Yff+04i5WmrrL1ki334QeKSHYa2iFcUqix4dyjS8GO5g==

// %URL%
// http://www.gnu.org/software/bash/bash.html

// %LICENSE%
// GPL

// %ARCH%
// x86_64

// %BUILDDATE%
// 1528102451

// %PACKAGER%
// Bartlomiej Piotrowski <bpiotrowski@archlinux.org>

// %PROVIDES%
// sh

// %DEPENDS%
// readline>=7.0
// glibc
// ncurses

// %OPTDEPENDS%
// bash-completion: for tab completion

// All fields used in the "extra" database

// TODO cross-reference with PKGBUILD docs and repositories built with repose/aurutils
// $ grep -hR '^%.*%' . | sort -u
// %ARCH%
// %BASE%
// %BUILDDATE%
// %CHECKDEPENDS%
// %CONFLICTS%
// %CSIZE%
// %DEPENDS%
// %DESC%
// %FILENAME%
// %GROUPS%
// %ISIZE%
// %LICENSE%
// %MAKEDEPENDS%
// %MD5SUM%
// %NAME%
// %OPTDEPENDS%
// %PACKAGER%
// %PGPSIG%
// %PROVIDES%
// %REPLACES%
// %SHA256SUM%
// %URL%
// %VERSION%
