package repositories_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/craigfurman/aurite/repositories"
)

func TestMarshallingValidPackages(t *testing.T) {
	for _, tc := range []struct {
		name                      string
		pkg                       repositories.Package
		expectedMarshalledPackage string
	}{
		{
			name:                      "package with all fields populated",
			pkg:                       packageWithAllFieldsPopulated,
			expectedMarshalledPackage: marshalledPackageWithAllFieldsPopulated,
		},
		{
			name:                      "omits empty fields",
			pkg:                       packageWithOneFieldPopulated,
			expectedMarshalledPackage: marshalledPackageWithOneFieldPopulated,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			marshalledPackage, err := repositories.MarshalPackage(tc.pkg)
			assert.Nil(t, err)
			assert.Equal(t, tc.expectedMarshalledPackage, string(marshalledPackage))
		})
	}
}

func TestParsePackageNameAndVersionOnValidMarshalledPackages(t *testing.T) {
	name, version, err := repositories.ParsePackageNameAndVersion([]byte(marshalledPackageWithAllFieldsPopulated))
	assert.Nil(t, err)
	assert.Equal(t, "name", name)
	assert.Equal(t, "version", version)
}

func TestParsePackageNameAndVersionOnInvalidMarshalledPackages(t *testing.T) {
	for _, tc := range []struct {
		name              string
		marshalledPackage string
		expectedErrMsg    string
	}{
		{
			name:              "missing version",
			marshalledPackage: "%NAME%\nfoo\n\n",
			expectedErrMsg:    "invalid package: missing version: %s",
		},
		{
			name:              "missing name",
			marshalledPackage: "%VERSION%\nfoo\n\n",
			expectedErrMsg:    "invalid package: missing name: %s",
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			_, _, err := repositories.ParsePackageNameAndVersion([]byte(tc.marshalledPackage))
			assert.EqualError(t, err, fmt.Sprintf(tc.expectedErrMsg, tc.marshalledPackage))
		})
	}
}

var packageWithAllFieldsPopulated = repositories.Package{
	PGPSig:       "pgpsig",
	Arch:         "arch",
	Base:         "base",
	BuildDate:    time.Date(2018, 8, 30, 9, 11, 59, 0, time.UTC),
	CSize:        1000000,
	CheckDepends: []string{"checkdepends1", "checkdepends2"},
	Conflicts:    []string{"conflicts1", "conflicts2"},
	Depends:      []string{"depends1", "depends2"},
	Desc:         "desc",
	Filename:     "name-version-arch.pkg.tar.xz",
	Groups:       []string{"groups1", "groups2"},
	ISize:        7000000,
	License:      "license",
	MakeDepends:  []string{"makedepends1", "makedepends2"},
	Md5Sum:       "md5sum",
	Name:         "name",
	OptDepends:   []string{"optdepends1: reason", "optdepends2: reason"},
	Packager:     "packager",
	Provides:     []string{"provides1", "provides2"},
	Replaces:     []string{"replaces1", "replaces2"},
	Sha256Sum:    "sha256sum",
	URL:          "url",
	Version:      "version",
}

var packageWithOneFieldPopulated = repositories.Package{Name: "name"}

const marshalledPackageWithAllFieldsPopulated = `%ARCH%
arch

%BASE%
base

%BUILDDATE%
1535620319

%CSIZE%
1000000

%CHECKDEPENDS%
checkdepends1
checkdepends2

%CONFLICTS%
conflicts1
conflicts2

%DEPENDS%
depends1
depends2

%DESC%
desc

%FILENAME%
name-version-arch.pkg.tar.xz

%GROUPS%
groups1
groups2

%ISIZE%
7000000

%LICENSE%
license

%MAKEDEPENDS%
makedepends1
makedepends2

%MD5SUM%
md5sum

%NAME%
name

%OPTDEPENDS%
optdepends1: reason
optdepends2: reason

%PGPSIG%
pgpsig

%PACKAGER%
packager

%PROVIDES%
provides1
provides2

%REPLACES%
replaces1
replaces2

%SHA256SUM%
sha256sum

%URL%
url

%VERSION%
version

`

const marshalledPackageWithOneFieldPopulated = `%NAME%
name

`
