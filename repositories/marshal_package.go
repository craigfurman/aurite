package repositories

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// MarshalPackage serialises Package objects to Arch Linux repository format.
// The order of fields in the marshalled package is in the alphabetical order of the
// corresponding Go struct field names.
func MarshalPackage(pkg Package) ([]byte, error) {
	marshalledPackage := ""

	pkgVal := reflect.ValueOf(pkg)
	pkgType := pkgVal.Type()
	for i := 0; i < pkgVal.NumField(); i++ {
		fieldName := pkgType.Field(i).Name
		fieldValue, err := marshalField(pkgVal.Field(i).Interface())
		if err != nil {
			return nil, err
		}

		if fieldValue != "" {
			marshalledPackage += fmt.Sprintf("%%%s%%\n%s\n\n", strings.ToUpper(fieldName), fieldValue)
		}
	}

	return []byte(marshalledPackage), nil
}

// ParsePackageNameAndVersion sort-of-unmarshals packages, poorly. I currently only need
// to read the name and version of marshalled packages from the repository file, and do
// not foresee needing to read anything else, so I haven't implemented it.
func ParsePackageNameAndVersion(marshalledPackage []byte) (string, string, error) {
	kvPairs := strings.Split(string(marshalledPackage), "\n\n")
	var name, version string
	for _, kvPair := range kvPairs {
		if strings.HasPrefix(kvPair, "%NAME%") {
			name = secondLine(kvPair)
		}
		if strings.HasPrefix(kvPair, "%VERSION%") {
			version = secondLine(kvPair)
		}
	}

	// If a package is missing both name and version, then the "missing name" error will
	// mask the "missing version" one. This is a limitation that is not hard to fix, but
	// this problem is unlikely to occur so I can't be bothere to implement it yet.
	if name == "" {
		return "", "", fmt.Errorf("invalid package: missing name: %s", string(marshalledPackage))
	}
	if version == "" {
		return "", "", fmt.Errorf("invalid package: missing version: %s", string(marshalledPackage))
	}

	return name, version, nil
}

func marshalField(field interface{}) (string, error) {
	switch val := field.(type) {
	case string:
		return val, nil
	case int64:
		if val == 0 {
			return "", nil
		}
		return strconv.FormatInt(val, 10), nil
	case []string:
		return strings.Join(val, "\n"), nil
	case time.Time:
		if val == (time.Time{}) {
			return "", nil
		}
		return marshalField(val.Unix())
	default:
		return "", fmt.Errorf("unsupported type for value %v", val)
	}
}

func secondLine(s string) string {
	return strings.Split(s, "\n")[1]
}
