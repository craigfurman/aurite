package repositories_test

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/craigfurman/aurite/repositories"
)

func TestAppendPackageDatabaseEntryWritesTarEntry(t *testing.T) {
	pkg := repositories.Package{Name: "pkg1", Version: "v1"}
	packageDB := repositories.PackageDatabase{MarshalPackage: fakeMarshalPackage}

	var buf bytes.Buffer
	tarW := tar.NewWriter(&buf)
	assert.Nil(t, packageDB.AppendPackageDatabaseEntry(pkg, tarW))
	assert.Nil(t, tarW.Close())

	tarR := tar.NewReader(&buf)
	assertTarEntries(t, tarR, pkg)
	assertTarFinished(t, tarR)
}

func TestInsertCreatesDatabaseFile(t *testing.T) {
	tmpDir, cleanup := createTempDir(t)
	defer cleanup()

	dbFilePath := filepath.Join(tmpDir, "some-repo.db")
	db := &repositories.PackageDatabase{
		Path:                       dbFilePath,
		MarshalPackage:             fakeMarshalPackage,
		ParsePackageNameAndVersion: fakeParsePackageNameAndVersion,
	}
	pkg := repositories.Package{Name: "pkg", Version: "1"}
	assert.Nil(t, db.Insert(pkg))

	tarR, closeDB := openDatabaseFile(t, dbFilePath)
	defer closeDB()
	assertTarEntries(t, tarR, pkg)
	assertTarFinished(t, tarR)
}

func TestInsertAppendsToExistingDatabaseTar(t *testing.T) {
	tmpDir, cleanup := createTempDir(t)
	defer cleanup()

	dbFilePath := filepath.Join(tmpDir, "some-repo.db")
	db := &repositories.PackageDatabase{
		Path:                       dbFilePath,
		MarshalPackage:             fakeMarshalPackage,
		ParsePackageNameAndVersion: fakeParsePackageNameAndVersion,
	}
	pkg1 := repositories.Package{Name: "pkg1", Version: "1"}
	assert.Nil(t, db.Insert(pkg1))

	pkg2 := repositories.Package{Name: "pkg2", Version: "1"}
	assert.Nil(t, db.Insert(pkg2))

	tarR, closeDB := openDatabaseFile(t, dbFilePath)
	defer closeDB()
	assertTarEntries(t, tarR, pkg1, pkg2)
	assertTarFinished(t, tarR)
}

func TestInsertReplacesPreviousPackageVersions(t *testing.T) {
	tmpDir, cleanup := createTempDir(t)
	defer cleanup()

	dbFilePath := filepath.Join(tmpDir, "some-repo.db")
	db := &repositories.PackageDatabase{
		Path:                       dbFilePath,
		MarshalPackage:             fakeMarshalPackage,
		ParsePackageNameAndVersion: fakeParsePackageNameAndVersion,
	}
	oldPkgVersion := repositories.Package{Name: "pkg1", Version: "1"}
	assert.Nil(t, db.Insert(oldPkgVersion))

	newPkgVersion := repositories.Package{Name: "pkg1", Version: "1.0.1"}
	assert.Nil(t, db.Insert(newPkgVersion))

	tarR, closeDB := openDatabaseFile(t, dbFilePath)
	defer closeDB()
	assertTarEntries(t, tarR, newPkgVersion)
	assertTarFinished(t, tarR)
}

func assertTarEntries(t *testing.T, reader *tar.Reader, pkgs ...repositories.Package) {
	for _, pkg := range pkgs {
		header, err := reader.Next()
		assert.Nil(t, err)
		assert.Equal(t, fmt.Sprintf("%s-%s/desc", pkg.Name, pkg.Version), header.Name)
		assert.EqualValues(t, len(expectedMarshalledPackage(pkg)), header.Size)

		entryBytes, err := ioutil.ReadAll(reader)
		assert.Nil(t, err)
		assert.Equal(t, string(entryBytes), string(expectedMarshalledPackage(pkg)))
	}
}

func assertTarFinished(t *testing.T, reader *tar.Reader) {
	_, err := reader.Next()
	assert.Equal(t, io.EOF, err)
}

func createTempDir(t *testing.T) (string, func()) {
	path, err := ioutil.TempDir("", "repositories_test")
	assert.Nil(t, err)
	return path, func() {
		assert.Nil(t, os.RemoveAll(path))
	}
}

func fakeMarshalPackage(pkg repositories.Package) ([]byte, error) {
	return []byte(fmt.Sprintf("%s-%s", pkg.Name, pkg.Version)), nil
}

func fakeParsePackageNameAndVersion(marshalledPackage []byte) (string, string, error) {
	nameDashVersion := strings.Split(string(marshalledPackage), "-")
	return nameDashVersion[0], nameDashVersion[1], nil
}

// expectedMarshalledPackage returns the stubbed marshalled package produced by
// fakeMarshalPackage. An error is never returned from that function.
func expectedMarshalledPackage(pkg repositories.Package) []byte {
	marshalledPackageBytes, _ := fakeMarshalPackage(pkg)
	return marshalledPackageBytes
}

func openDatabaseFile(t *testing.T, dbFilePath string) (*tar.Reader, func()) {
	f, err := os.Open(dbFilePath)
	assert.Nil(t, err)
	gzipR, err := gzip.NewReader(f)
	assert.Nil(t, err)
	tarR := tar.NewReader(gzipR)
	return tarR, func() {
		// *tar.Reader is not a Closer
		assert.Nil(t, gzipR.Close())
		assert.Nil(t, f.Close())
	}
}
