package repositories

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

// TODO proper logging, in logfmt

// TODO refactor PackageDatabase to take an interface dependency?

type PackageDatabase struct {
	Path                       string
	MarshalPackage             func(pkg Package) ([]byte, error)
	ParsePackageNameAndVersion func(marshalledPackage []byte) (string, string, error)
}

func (p *PackageDatabase) Insert(pkg Package) (err error) {
	tmpDBPath := p.Path + ".tmp"
	tmpDBFile, err := os.OpenFile(tmpDBPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	gzipW := gzip.NewWriter(tmpDBFile)
	tarW := tar.NewWriter(gzipW)
	defer func() {
		// Intentionally reassign err var: we are in a defer, and want to influence the return
		// value.
		if err = tarW.Close(); err != nil {
			return
		}
		if err = gzipW.Close(); err != nil {
			return
		}
		if err = tmpDBFile.Close(); err != nil {
			return
		}
	}()

	if err := p.copyExistingDatabase(tarW, pkg.Name); err != nil {
		return err
	}
	if err := p.AppendPackageDatabaseEntry(pkg, tarW); err != nil {
		return err
	}

	// Writing the new database to a new file before relinking it is necessary to prevent
	// corruption of the existing database in case of error. This is not explicitly tested.
	return os.Rename(tmpDBPath, p.Path)
}

func (p *PackageDatabase) copyExistingDatabase(newDbWriter *tar.Writer, excludePackage string) (result error) {
	if _, err := os.Stat(p.Path); err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}

	oldDbTarR, oldDbReaderCleanup, err := p.openDatabaseFile()
	if err != nil {
		return err
	}
	defer func() {
		result = oldDbReaderCleanup()
	}()

	for {
		oldDbHeader, err := oldDbTarR.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		pkgBytes, err := ioutil.ReadAll(oldDbTarR)
		if err != nil {
			return err
		}

		if err := p.copyUnexcludedPackage(oldDbHeader, pkgBytes, newDbWriter, excludePackage); err != nil {
			return err
		}
	}

	return nil
}

func (p *PackageDatabase) copyUnexcludedPackage(oldDbHeader *tar.Header, pkgBytes []byte, newDbWriter *tar.Writer, excludePackage string) error {
	pkgName, _, err := p.ParsePackageNameAndVersion(pkgBytes)
	if err != nil {
		return err
	}
	if pkgName != excludePackage {
		if err := copyTarEntry(oldDbHeader, pkgBytes, newDbWriter); err != nil {
			return err
		}
	}

	return nil
}

func copyTarEntry(header *tar.Header, entry []byte, tarW *tar.Writer) error {
	if err := tarW.WriteHeader(header); err != nil {
		return err
	}
	if _, err := tarW.Write(entry); err != nil {
		return err
	}
	return nil
}

func (p *PackageDatabase) openDatabaseFile() (*tar.Reader, func() error, error) {
	oldDbFile, err := os.Open(p.Path)
	if err != nil {
		return nil, nil, err
	}
	oldDbGzipR, err := gzip.NewReader(oldDbFile)
	if err != nil {
		return nil, nil, err
	}
	return tar.NewReader(oldDbGzipR), func() error {
		if err := oldDbGzipR.Close(); err != nil {
			return err
		}
		return oldDbFile.Close()
	}, nil
}

func (p *PackageDatabase) AppendPackageDatabaseEntry(pkg Package, packageDbWriter *tar.Writer) error {
	marshalledPkg, err := p.MarshalPackage(pkg)
	if err != nil {
		return err
	}

	header := &tar.Header{
		Name: fmt.Sprintf("%s-%s/desc", pkg.Name, pkg.Version),
		Size: int64(len(marshalledPkg)),
	}

	if err := packageDbWriter.WriteHeader(header); err != nil {
		return err
	}
	if _, err := packageDbWriter.Write(marshalledPkg); err != nil {
		return err
	}

	return nil
}
